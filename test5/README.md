# 软工一班 瞿勇 202010111513 

## 实验名称

包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

## 实验步骤 

###  创建包

```
SQL> create or replace PACKAGE MyPack IS
  2  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  3  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
  4  END MyPack;
  5  /
```

![](pict1.png)

### 创建函数

![](pict2.png)

### 执行函数查询

```
SQL> select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


SQL> set serveroutput on
SQL> DECLARE
  2  V_EMPLOYEE_ID NUMBER; 
  3  BEGIN
  4  V_EMPLOYEE_ID := 101;
  5  MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID); 
  6  END;
  7  /

```

![](pict3.png)

![](pict4.png)
