## 软工一班 瞿勇 202010111513

## 实验名称

基于Oracle数据库的商品销售系统的设计

## 实验内容

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验步骤

### 1.创建表空间

- 创建普通用户的表空间

  ```sql
  CREATE TABLESPACE user_space DATAFILE '/home/oracle/data/user_space' size 200M EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
  ```

  ![image-20230526102536674](./img/image-20230526102536674.png)

- 创建管理员用户的表空间

	```sql
	CREATE TABLESPACE admin_space DATAFILE '/home/oracle/data/admin_space' size 200M EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
	```
  
	![image-20230526102659413](./img/image-20230526102659413.png)

### 2.建表

- 创建商品-鲜花表

  ```sql
  create table flower(
  	fno varchar2(6) primary key, --鲜花编号
  	fname varchar2(30) not null, --鲜花名
  	fcolor varchar2(10), --鲜花颜色
  	price float, --鲜花价格
  	fsay varchar2(30)  --鲜花花语
  )	tablespace user_space;
  ```

  ![image-20230526102910723](./img/image-20230526102910723.png)

- 创建顾客表

  ```sql
  create table customer(
  	cuno varchar2(5) primary key, --顾客编号
  	cuname varchar2(30) not null, --顾客姓名
  	address varchar2(30) --顾客地址
  )	tablespace user_space;
  ```

  ![image-20230526102952959](./img/image-20230526102952959.png)

- 创建销售表

  ```sql
  create table sale(
  	fno varchar2(6), --鲜花编号
  	cuno varchar2(5), --顾客编号
  	saledate date, --销售日期
  	saleprice float  --销售价格
  )	tablespace admin_space;
  ```

  ![image-20230526103034130](./img/image-20230526103034130.png)

- 创建销售统计表

  ```sql
  create table saleTotal(
  	cuname varchar2(30) not null, --顾客姓名
  	fname varchar2(30) not null, --鲜花名
  	saleTotal float --销售总计
  )	tablespace user_space;
  ```

  ![image-20230526103134981](./img/image-20230526103134981.png)
  
  ### 3.插入模拟数据

- 商品-鲜花表插入数据

  ```sql
  insert into flower values('0001','玫瑰','red',6,'我爱你');
  insert into flower values('0002','百合','white',8,'健康快乐');
  insert into flower values('0003','康乃馨','pink',5,'无私的爱');
  insert into flower values('0004','满天星','blue',4,'甘愿做你的配角');
  insert into flower values('0005','雏菊','yellow',7,'隐藏在心中的爱');
  insert into flower values('0006','忘忧草','yellow',8,'忘记烦恼，快乐相随');
  insert into flower values('0007','有钱花','red',100,'最快乐');
  ```

  ![image-20230526103209430](./img/image-20230526103209430.png)

- 顾客表插入数据

  ```sql
  insert into customer values('1001','孙悟空','花果山');
  insert into customer values('1002','大耳朵图图','上海');
  insert into customer values('1003','刘星','北京');
  insert into customer values('1004','柯南','日本');
  insert into customer values('1005','熊大','狗熊岭');
  insert into customer values('1006','熊二','狗熊岭');
  insert into customer values('1007','光头强','狗熊岭');
  insert into customer values('1008','t1','linux');
  ```

  ![image-20230526103351210](./img/image-20230526103351210.png)

- 销售表创建存储过程test

  ```sql
  create or replace procedure test is    --存储过程，名称为test
    v_id date;   --声明变量
  begin
    v_id := to_date('2000-1-1','yyyy-mm-dd');    
    while v_id <= to_date('2023-5-25','yyyy-mm-dd') 
     loop
      insert into sale  values ('0001','1001',v_id,100);
  	insert into sale  values ('0001','1003',v_id,100);
  	insert into sale  values ('0001','1005',v_id,100);
  	insert into sale  values ('0001','1008',v_id,100);
  	insert into sale  values ('0002','1002',v_id,100);
  	insert into sale  values ('0002','1004',v_id,100);
  	insert into sale  values ('0002','1005',v_id,100);
  	insert into sale  values ('0002','1006',v_id,100);
  	insert into sale  values ('0002','1007',v_id,100);
  	insert into sale  values ('0003','1001',v_id,100);
  	insert into sale  values ('0001','1003',v_id,100);
  	insert into sale  values ('0001','1004',v_id,100);
  	insert into sale  values ('0001','1005',v_id,100);
  	insert into sale  values ('0004','1002',v_id,100);
  	insert into sale  values ('0004','1006',v_id,100);
  	insert into sale  values ('0005','1007',v_id,100);
  	insert into sale  values ('0005','1008',v_id,100);
  	insert into sale  values ('0006','1002',v_id,100);
  	insert into sale  values ('0006','1004',v_id,100);
  	insert into sale  values ('0007','1008',v_id,100);
      v_id := v_id + 1;   --循环时每次加一
    end loop;
    commit;
  end;
  /
  ```

  ![image-20230526103707893](./img/image-20230526103707893.png)

  执行存储过程，批量插入数据

  ```sql
  exec test
  ```

  ![image-20230526103729743](./img/image-20230526103729743.png)

### 4.创建用户并分配权限

- 创建普通用户u1，授予销售表sale的插入权限

  ```sql
  create user u1 identified by 123456;
  grant connect,resource to u1;
  grant insert on sale to u1;
  ```

  ![image-20230526103803876](./img/image-20230526103803876.png)

- 创建管理员用户admin，授予销售统计表saleTotal的所有权限

  ```sql
  create user admin identified by 123456;
  grant connect,resource to admin;
  grant insert,delete on saleTotal to admin;
  ```

  ![image-20230526103830483](./img/image-20230526103830483.png)

### 5.建立程序包

- 声明程序包，包含存储过程InsertSalTotal和函数SalTotalByCustName，其中存储过程InsertSalTotal的作用为更新销售统计表的数据，函数SalTotalByCustName的作用为根据顾客名查询其消费的总额

  ```sql
  create or replace package admin_mangent
  
  as
  
      procedure InsertSalTotal;
  
      function SalTotalByCustName(cuname in varchar2) return number;
  
  end admin_mangent;
  /
  ```

  ![image-20230526103907609](./img/image-20230526103907609.png)

- 创建程序包体

  ```sql
  create or replace package body admin_mangent
  
  as
  
      procedure InsertSalTotal
       as
       begin
  		delete from saleTotal;
  		insert into saleTotal (select cuname,fname,saleTotal 
  		from flower,customer,
  			(select cuno,fno,sum(saleprice) saleTotal
  				from sale
  				group by cuno,fno ) S
  		where S.cuno = customer.cuno 
  			and S.fno = flower.fno);
  		
  		commit;
       end InsertSalTotal;
  
       function SalTotalByCustName(cuname in varchar2) return number
       as
          Total number;
       begin
  
          select sum(saleTotal) into Total from saleTotal where cuname = cuname;
  
           return Total;
  
       end SalTotalByCustName;
  
  end admin_mangent;
  /
  ```

  ![image-20230526104130665](./img/image-20230526104130665.png)

  - 调用存储过程

    ```sql
    exec admin_mangent.InsertSalTotal;
    ```

    ![image-20230526104148563](./img/image-20230526104148563.png)

  - 调用函数

    ```sql
    select admin_mangent.SalTotalByCustName('孙悟空') from dual;
    ```

    ![image-20230526104202460](./img/image-20230526104202460.png)

### 6.备份

将数据库进行完全导入

```
exp system/manager inctype=complete file=230526.dmp
```

![image-20230526104416653](./img/image-20230526104416653.png)















