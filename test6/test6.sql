1. 创建表空间

- 创建普通用户的表空间
CREATE TABLESPACE user_space DATAFILE '/home/oracle/data/user_space' size 200M EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

- 创建管理员用户的表空间
CREATE TABLESPACE admin_space DATAFILE '/home/oracle/data/admin_space' size 200M EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

2. 建表

- 创建商品-鲜花表
create table flower(
  	fno varchar2(6) primary key, --鲜花编号
  	fname varchar2(30) not null, --鲜花名
  	fcolor varchar2(10), --鲜花颜色
  	price float, --鲜花价格
  	fsay varchar2(30)  --鲜花花语
)	tablespace user_space;

- 创建顾客表
create table customer(
  	cuno varchar2(5) primary key, --顾客编号
  	cuname varchar2(30) not null, --顾客姓名
  	address varchar2(30) --顾客地址
)	tablespace user_space;

- 创建销售表
create table sale(
  	fno varchar2(6), --鲜花编号
  	cuno varchar2(5), --顾客编号
  	saledate date, --销售日期
  	saleprice float  --销售价格
)	tablespace admin_space;

- 创建销售统计表
create table saleTotal(
  	cuname varchar2(30) not null, --顾客姓名
  	fname varchar2(30) not null, --鲜花名
  	saleTotal float --销售总计
)	tablespace user_space;

3. 插入模拟数据

- 商品-鲜花表插入数据
insert into flower values('0001','玫瑰','red',6,'我爱你');
insert into flower values('0002','百合','white',8,'健康快乐');
insert into flower values('0003','康乃馨','pink',5,'无私的爱');
insert into flower values('0004','满天星','blue',4,'甘愿做你的配角');
insert into flower values('0005','雏菊','yellow',7,'隐藏在心中的爱');
insert into flower values('0006','忘忧草','yellow',8,'忘记烦恼，快乐相随');
insert into flower values('0007','有钱花','red',100,'最快乐');

- 顾客表插入数据
insert into customer values('1001','孙悟空','花果山');
insert into customer values('1002','大耳朵图图','上海');
insert into customer values('1003','刘星','北京');
insert into customer values('1004','柯南','日本');
insert into customer values('1005','熊大','狗熊岭');
insert into customer values('1006','熊二','狗熊岭');
insert into customer values('1007','光头强','狗熊岭');
insert into customer values('1008','t1','linux');

- 销售表创建存储过程test
create or replace procedure test is    --存储过程，名称为test
  v_id date;   --声明变量
begin
  v_id := to_date('2000-1-1','yyyy-mm-dd');    
  while v_id <= to_date('2023-5-25','yyyy-mm-dd') 
   loop
    insert into sale  values ('0001','1001',v_id,100);
	insert into sale  values ('0001','1003',v_id,100);
	insert into sale  values ('0001','1005',v_id,100);
	insert into sale  values ('0001','1008',v_id,100);
	insert into sale  values ('0002','1002',v_id,100);
	insert into sale  values ('0002','1004',v_id,100);
	insert into sale  values ('0002','1005',v_id,100);
	insert into sale  values ('0002','1006',v_id,100);
	insert into sale  values ('0002','1007',v_id,100);
	insert into sale  values ('0003','1001',v_id,100);
	insert into sale  values ('0001','1003',v_id,100);
	insert into sale  values ('0001','1004',v_id,100);
	insert into sale  values ('0001','1005',v_id,100);
	insert into sale  values ('0004','1002',v_id,100);
	insert into sale  values ('0004','1006',v_id,100);
	insert into sale  values ('0005','1007',v_id,100);
	insert into sale  values ('0005','1008',v_id,100);
	insert into sale  values ('0006','1002',v_id,100);
	insert into sale  values ('0006','1004',v_id,100);
	insert into sale  values ('0007','1008',v_id,100);
    v_id := v_id + 1;   --循环时每次加一
  end loop;
  commit;
end;
/

执行存储过程，批量插入数据
exec test

4. 创建用户并分配权限

- 创建普通用户u1，授予销售表sale的插入权限
create user u1 identified by 123456;
grant connect,resource to u1;
grant insert on sale to u1;

- 创建管理员用户admin，授予销售统计表saleTotal的所有权限
create user admin identified by 123456;
grant connect,resource to admin;
grant insert,delete on saleTotal to admin;

5. 建立程序包

- 声明程序包，包含存储过程InsertSalTotal和函数SalTotalByCustName，其中存储过程InsertSalTotal的作用为更新销售统计表的数据，函数SalTotalByCustName的作用为根据顾客名查询其消费的总额
create or replace package admin_mangent

as

    procedure InsertSalTotal;

    function SalTotalByCustName(cuname in varchar2) return number;

end admin_mangent;
/

- 创建程序包体
create or replace package body admin_mangent

as

    procedure InsertSalTotal
     as
     begin
		delete from saleTotal;
		insert into saleTotal (select cuname,fname,saleTotal 
		from flower,customer,
			(select cuno,fno,sum(saleprice) saleTotal
				from sale
				group by cuno,fno ) S
		where S.cuno = customer.cuno 
			and S.fno = flower.fno);
		
		commit;
     end InsertSalTotal;

     function SalTotalByCustName(cuname in varchar2) return number
     as
        Total number;
     begin

        select sum(saleTotal) into Total from saleTotal where cuname = cuname;

         return Total;

     end SalTotalByCustName;

end admin_mangent;
/
  

- 调用存储过程
exec admin_mangent.InsertSalTotal;

- 调用函数
select admin_mangent.SalTotalByCustName('孙悟空') from dual;

6. 备份
将数据库进行完全导入
exp system/manager inctype=complete file=230526.dmp
